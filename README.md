OpenAI API Wrapper for Advanced ChatGPT Development
===================================================

[中文说明](README_cn.md)

This Python package provides a comprehensive and efficient wrapper for the OpenAI API, designed to support advanced application development based on ChatGPT. The package simplifies the integration process and offers additional functionalities such as conversation management, model fine-tuning, retrieval of embeddings, automatic question generation, and offline model execution.

Features
--------

* Easy interaction with the OpenAI API for ChatGPT applications
* Conversation management for multiple chat sessions
* Support for fine-tuning the ChatGPT model
* Retrieval of embeddings for specific text passages
* Automatic question generation for given paragraphs
* Offline model execution with compatible models

Installation
------------

You can install the package using pip:

`pip install openai-api-wrapper`

Quick Start
-----------

```python
from openai_api_wrapper import ChatBot

# Initialize the Chatbot instance
# Set environment variable OPENAI_API_KEY to your OpenAI API key.
# Set environment variable OPENAI_PROXY to your OpenAI proxy setting, 
# such as "http://<proxy_server>:<proxy_port>" or "socks5://<proxy_server>:<proxy_port>" .
chatbot = ChatBot()
# or 
# chatbot = ChatBot(api_key="your_api_key", proxy="your_proxy")

reply_content= chatbot.ask(prompt)
```

Usage
-----

Here is a basic example of how to use the OpenAI API Wrapper:

```python
"""
Set environment variable OPENAI_API_KEY to your OpenAI API key.
Set environment variable OPENAI_PROXY to your OpenAI proxy setting, 
such as "http://<proxy_server>:<proxy_port>" or "socks5://<proxy_server>:<proxy_port>" .
"""
from openai_api_wrapper import ChatBot

# Initialize the Chatbot instance
chatbot = ChatBot()

def show_result(conversation_id, reply_content):
    print(f"[{conversation_id}] AI Response: {reply_content}")
    conversation_turns = chatbot.get_conversation_turns(conversation_id=conversation_id)
    print(f"[{conversation_id}] Conversation turns: {conversation_turns}")

# ----- The default conversation -----

# Generate a reply to the given prompt.
prompt = "What is the capital of France?"
print(f"[default] User message: {prompt}")
reply_content= chatbot.ask(prompt)
show_result("default", reply_content)

# Generate a reply to the given prompt using streaming API.
prompt = "Tell me a joke."
print(f"[default] User message: {prompt}")
reply_content= chatbot.ask(prompt, stream=True)
show_result("default", reply_content)

# ----- Two conversations -----

# Generate a reply_contentto the given prompt.
conversation_id = "conversation_1"
prompt = "What is the capital of France?"
print(f"[{conversation_id}] User message: {prompt}")

reply_content= chatbot.ask(prompt, conversation_id=conversation_id)
show_result(conversation_id, reply_content)


# Generate a reply_contentto the given prompt using streaming API.
conversation_id = "conversation_2"
prompt = "Tell me a joke."
print(f"[{conversation_id}] User message: {prompt}")

reply_content= chatbot.ask(prompt, conversation_id=conversation_id, stream=True)
# unfinished_reply_content= ""
# for chunked_reply_content in chatbot.ask_stream_iterator(prompt, conversation_id=conversation_id):
#     unfinished_reply_content+= chunked_reply_content
#     print(unfinished_reply_content)
# reply_content= chatbot.get_last_reply_content(conversation_id)

show_result(conversation_id, reply_content)
```

**Output**

```bash
[default] User message: What is the capital of France?
[default] AI Response: The capital of France is Paris.
[default] Conversation turns: 1
[default] User message: Tell me a joke.

Why
Why was
Why was the
Why was the math
Why was the math book
Why was the math book sad
Why was the math book sad?
Why was the math book sad? Because
Why was the math book sad? Because it
Why was the math book sad? Because it had
Why was the math book sad? Because it had too
Why was the math book sad? Because it had too many
Why was the math book sad? Because it had too many problems
Why was the math book sad? Because it had too many problems.
Why was the math book sad? Because it had too many problems.
[default] AI Response: Why was the math book sad? Because it had too many problems.
[default] Conversation turns: 2
[conversation_1] User message: What is the capital of France?
[conversation_1] AI Response: The capital of France is Paris.
[conversation_1] Conversation turns: 1
[conversation_2] User message: Tell me a joke.

Why
Why did
Why did the
Why did the tomato
Why did the tomato turn
Why did the tomato turn red
Why did the tomato turn red?
Why did the tomato turn red? Because
Why did the tomato turn red? Because it
Why did the tomato turn red? Because it saw
Why did the tomato turn red? Because it saw the
Why did the tomato turn red? Because it saw the salad
Why did the tomato turn red? Because it saw the salad dressing
Why did the tomato turn red? Because it saw the salad dressing!
Why did the tomato turn red? Because it saw the salad dressing!
[conversation_2] AI Response: Why did the tomato turn red? Because it saw the salad dressing!
[conversation_2] Conversation turns: 1
```

For a more detailed example with all the available features, check out the `example.py` file in the repository.

Documentation
-------------

You can find the complete documentation for this package in the `docs` folder, or check out the source code for more details on the implementation.

Roadmap
-------

We plan to continually improve and expand the functionality of this package. Some of the upcoming features include:

* Integration with various machine learning frameworks
* Support for multi-modal inputs (e.g., text, images, audio)
* Expansion of available pre-trained models
* Simplified deployment options for various platforms

Contributing
------------

We welcome contributions from the community! If you'd like to contribute, please follow these steps:

1. Fork the repository
2. Create a new branch for your changes (`git checkout -b my-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push the branch (`git push origin my-feature`)
5. Create a new pull request

Please make sure to add tests and documentation for any new features or changes.

License
-------

This project is licensed under the MIT License. See the `LICENSE` file for more details.
