用于高级ChatGPT应用开发的 OpenAI API 封装库
==========================
[English Version](README.md)

这个 Python 包为 OpenAI API 提供了一个全面且高效的封装，旨在支持基于 ChatGPT 的高级应用开发。该包简化了集成过程，并提供了额外的功能，如会话管理、模型微调、检索嵌入、自动问题生成和离线模型执行。

功能
--

* 简化与 OpenAI API 交互，支持ChatGPT高级应用开发
* 多个聊天会话的会话管理
* 支持 ChatGPT 模型的微调
* 检索特定文本段落的嵌入
* 根据给定段落自动生成问题
* 与兼容模型一起进行离线模型执行

安装
--

您可以使用 pip 安装此包：

`pip install openai-api-wrapper`

快速启动
-------

```python
# 初始化 ChatBot 实例
# 设置环境变量 OPENAI_API_KEY 为你的 OpenAI API key 。
# 设置环境变量 OPENAI_PROXY 为你的 OpenAI 代理服务器设置，
# 比如："http://<proxy_server>:<proxy_port>" 或 "socks5://<proxy_server>:<proxy_port>" 。
chatbot = ChatBot()
# 或者 
# chatbot = ChatBot(api_key="your_api_key", proxy="your_proxy")

reply_content= chatbot.ask(prompt)
```

使用
--

以下是如何使用 OpenAI API 封装库的基本示例：

python

```python
"""
设置环境变量 OPENAI_API_KEY 为你的 OpenAI API key 。
设置环境变量 OPENAI_PROXY 为你的 OpenAI 代理服务器设置，
比如："http://<proxy_server>:<proxy_port>" 或 "socks5://<proxy_server>:<proxy_port>" 。
"""
from openai_api_wrapper import ChatBot

# 初始化 ChatBot 实例
chatbot = ChatBot()

def show_result(conversation_id, reply_content):
    print(f"[{conversation_id}] AI Response: {reply_content}")
    conversation_turns = chatbot.get_conversation_turns(conversation_id=conversation_id)
    print(f"[{conversation_id}] Conversation turns: {conversation_turns}")

# ----- 缺省会话 -----

# 生成给定提示的回复。
prompt = "法国的首都在哪里？"
print(f"[default] User message: {prompt}")
reply_content= chatbot.ask(prompt)
show_result("default", reply_content)

# 使用流式API生成给定提示的回复。
prompt = "给我说个笑话。"
print(f"[default] User message: {prompt}")
reply_content= chatbot.ask(prompt, stream=True)
show_result("default", reply_content)

# ----- 两个对话 -----

# 生成给定提示的回复。
conversation_id = "conversation_1"
prompt = "法国的首都在哪里？"
print(f"[{conversation_id}] User message: {prompt}")

reply_content= chatbot.ask(prompt, conversation_id=conversation_id)
show_result(conversation_id, reply_content)

# 使用流式API生成给定提示的回复。
conversation_id = "conversation_2"
prompt = "给我说个笑话。"
print(f"[{conversation_id}] User message: {prompt}")

reply_content= chatbot.ask(prompt, conversation_id=conversation_id, stream=True)
# unfinished_reply_content= ""
# for chunked_reply_content in chatbot.ask_stream_iterator(prompt, conversation_id=conversation_id):
#     unfinished_reply_content+= chunked_reply_content
#     print(unfinished_reply_content)
# reply_content= chatbot.get_last_reply_content(conversation_id)

show_result(conversation_id, reply_content)
```

**输出**

```bash
[default] User message: 法国的首都在哪里？
[default] AI Response: 法国的首都是巴黎。
[default] Conversation turns: 1
[default] User message: 给我说个笑话。

好
好的
好的，
好的，这
好的，这是
好的，这是一个
好的，这是一个冷
好的，这是一个冷笑
好的，这是一个冷笑话
好的，这是一个冷笑话：
好的，这是一个冷笑话：为
好的，这是一个冷笑话：为什
好的，这是一个冷笑话：为什么
好的，这是一个冷笑话：为什么猫
好的，这是一个冷笑话：为什么猫咪
好的，这是一个冷笑话：为什么猫咪喜
好的，这是一个冷笑话：为什么猫咪喜欢
好的，这是一个冷笑话：为什么猫咪喜欢玩
好的，这是一个冷笑话：为什么猫咪喜欢玩电
好的，这是一个冷笑话：为什么猫咪喜欢玩电脑
好的，这是一个冷笑话：为什么猫咪喜欢玩电脑？
好的，这是一个冷笑话：为什么猫咪喜欢玩电脑？因
好的，这是一个冷笑话：为什么猫咪喜欢玩电脑？因为
好的，这是一个冷笑话：为什么猫咪喜欢玩电脑？因为它
好的，这是一个冷笑话：为什么猫咪喜欢玩电脑？因为它们
好的，这是一个冷笑话：为什么猫咪喜欢玩电脑？因为它们喜
好的，这是一个冷笑话：为什么猫咪喜欢玩电脑？因为它们喜欢
好的，这是一个冷笑话：为什么猫咪喜欢玩电脑？因为它们喜欢用
好的，这是一个冷笑话：为什么猫咪喜欢玩电脑？因为它们喜欢用鼠
好的，这是一个冷笑话：为什么猫咪喜欢玩电脑？因为它们喜欢用鼠标
好的，这是一个冷笑话：为什么猫咪喜欢玩电脑？因为它们喜欢用鼠标！
好的，这是一个冷笑话：为什么猫咪喜欢玩电脑？因为它们喜欢用鼠标！
[default] AI Response: 好的，这是一个冷笑话：为什么猫咪喜欢玩电脑？因为它们喜欢用鼠标！
[default] Conversation turns: 2
...
```

有关所有可用功能的更详细示例，请查看存储库中的 `example.py` 文件。

文档
--

您可以在 `docs` 文件夹中找到此包的完整文档，或查看源代码以获取有关实现的更多详细信息。

开发路线图
-----

我们计划不断改进和扩展此包的功能。一些即将推出的功能包括：

* 与各种机器学习框架集成
* 支持多模态输入（例如，文本、图像、音频）
* 扩展可用的预训练模型
* 简化各种平台的部署选项

贡献
--

我们欢迎社区的贡献！如果您想做出贡献，请按照以下步骤操作：

1. Fork 仓库
2. 为更改创建新分支（`git checkout -b my-feature`）
3. 提交更改（`git commit -am 'Add some feature'`）
4. 推送分支（`git push origin my-feature`）
5. 创建新的拉取请求

请确保为任何新功能或更改添加测试和文档。

许可证
---

本项目采用 MIT 许可证。有关详细信息，请参阅 `LICENSE` 文件。
