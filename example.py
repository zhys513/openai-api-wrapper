"""
Set environment variable OPENAI_API_KEY to your OpenAI API key.
Set environment variable OPENAI_PROXY to your OpenAI proxy setting, 
such as "http://<proxy_server>:<proxy_port>" or "socks5://<proxy_server>:<proxy_port>" .
"""
from openai_api_wrapper import ChatBot
def main():
    # Initialize the Chatbot instance
    # chatbot = ChatBot(
    #     api_key="your_api_key_here",
    #     engine="gpt-3.5-turbo",
    #     temperature=0.5,
    #     max_tokens=3000,
    # )
    chatbot = ChatBot()

    def show_result(conversation_id, reply_content):
        print(f"[{conversation_id}] AI Response: {reply_content}")
        conversation_turns = chatbot.get_conversation_turns(conversation_id=conversation_id)
        print(f"[{conversation_id}] Conversation turns: {conversation_turns}")

    # ----- The default conversation -----

    # Generate a reply to the given prompt.
    prompt = "What is the capital of France?"
    print(f"[default] User message: {prompt}")
    reply_content= chatbot.ask(prompt)
    show_result("default", reply_content)

    # Generate a reply to the given prompt using streaming API.
    prompt = "Tell me a joke."
    print(f"[default] User message: {prompt}")
    reply_content= chatbot.ask(prompt, stream=True)
    show_result("default", reply_content)


    # ----- Two conversations -----

    # Generate a reply_contentto the given prompt.
    conversation_id = "conversation_1"
    prompt = "What is the capital of France?"
    print(f"[{conversation_id}] User message: {prompt}")

    reply_content= chatbot.ask(prompt, conversation_id=conversation_id)
    show_result(conversation_id, reply_content)


    # Generate a reply_contentto the given prompt using streaming API.
    conversation_id = "conversation_2"
    prompt = "Tell me a joke."
    print(f"[{conversation_id}] User message: {prompt}")

    reply_content= chatbot.ask(prompt, conversation_id=conversation_id, stream=True)
    # unfinished_reply_content= ""
    # for chunked_reply_content in chatbot.ask_stream_iterator(prompt, conversation_id=conversation_id):
    #     unfinished_reply_content+= chunked_reply_content
    #     print(unfinished_reply_content)
    # reply_content= chatbot.get_last_reply_content(conversation_id)

    show_result(conversation_id, reply_content)

    # # Get token count for a string
    # token_count = chatbot.get_token_count("This is an example sentence.")
    # print(f"Token count: {token_count}")

if __name__ == "__main__":
    main()

